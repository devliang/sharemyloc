package io.github.sharemyloc;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ShareActivity extends Activity {
    @Bind(R.id.shareUrlLabel) TextView shareUrlLabel;
    @Bind(R.id.expirationTimeLabel) TextView expirationTimeLabel;
    @Bind(R.id.subtract15MinutesButton) Button subtract15MinutesButton;
    @Bind(R.id.add15MinutesButton) Button add15MinutesButton;

    private static final DateFormat TIME_FORMATTER = DateFormat.getTimeInstance(DateFormat.SHORT);

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        ButterKnife.bind(this);
    }

    @Override protected void onResume() {
        super.onResume();

        shareUrlLabel.setText(SharingManager.getShareUrl());
        refreshExpirationViews();
    }

    @OnClick(R.id.stopSharingButton) public void stopSharing(View view) {
        FirebaseClient.getInstance(this).removeSession(SharingManager.getSessionId());
        SharingManager.stopSharing();
        super.onBackPressed();
    }

    @OnClick(R.id.copyUrlButton) public void copyLocationUrlToClipboard() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("URL", SharingManager.getShareUrl());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, R.string.copy_url_feedback, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.shareUrlViaButton) public void shareLocationUrlVia() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_url_action_body, SharingManager.getShareUrl()));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getString(R.string.share_url_action_title)));
    }

    @OnClick(R.id.subtract15MinutesButton) public void subtract15MinutesButton() {
        SharingManager.setExpiration(SharingManager.getExpiration() - DateUtils.MINUTE_IN_MILLIS * 15);
        refreshExpirationViews();
    }

    @OnClick(R.id.add15MinutesButton) public void add15Minutes() {
        SharingManager.setExpiration(SharingManager.getExpiration() + DateUtils.MINUTE_IN_MILLIS * 15);
        refreshExpirationViews();
    }

    private void refreshExpirationViews() {
        Date date = new Date(SharingManager.getExpiration());
        expirationTimeLabel.setText(TIME_FORMATTER.format(date));

        long duration = SharingManager.getExpiration() - System.currentTimeMillis();
        if (duration <= DateUtils.MINUTE_IN_MILLIS * 15) {
            subtract15MinutesButton.setEnabled(false);
        } else if (duration >= DateUtils.HOUR_IN_MILLIS * 4) {
            add15MinutesButton.setEnabled(false);
        } else {
            subtract15MinutesButton.setEnabled(true);
            add15MinutesButton.setEnabled(true);
        }
    }
}
