package io.github.sharemyloc;

import android.app.AlarmManager;
import android.content.Context;
import android.text.format.DateUtils;

import com.google.gson.JsonObject;

import java.util.UUID;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SharingManager {
    private static String sessionId = "";
    private static boolean isSharing = false;
    private static String shareUrl = "";
    private static long expiration = 0;

    private static final AlarmManager ALARM_MANAGER = (AlarmManager) App.getContext().getSystemService(Context.ALARM_SERVICE);

    public static String getSessionId() {
        return sessionId;
    }

    public static boolean isSharing() {
        return isSharing;
    }

    public static String getShareUrl() {
        return shareUrl;
    }

    public static long getExpiration() {
        return expiration;
    }

    public static void setExpiration(long value) {
        expiration = value;
        // TODO: set an alarm to stop sharing at expiration time
    }

    public static void startSharing(final CompletionCallback<Boolean> cb) {
        if (isSharing) {
            cb.onComplete(true);
            return;
        }

        sessionId = UUID.randomUUID().toString();

        JsonObject payload = new JsonObject();
        payload.addProperty("longUrl", "https://sharemyloc.github.io/?" + sessionId);
        UrlShortener.getService().shorten(payload, new Callback<JsonObject>() {
            @Override public void success(JsonObject body, Response response) {
                shareUrl = body.get("id").getAsString();
                expiration = System.currentTimeMillis() + DateUtils.HOUR_IN_MILLIS;
                isSharing = true;
                cb.onComplete(true);
            }

            @Override public void failure(RetrofitError error) {
                cb.onComplete(false);
            }
        });
    }

    public static void stopSharing() {
        sessionId = "";
        isSharing = false;
        shareUrl = "";
        expiration = 0;
    }
}
