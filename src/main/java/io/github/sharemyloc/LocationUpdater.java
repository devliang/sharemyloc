package io.github.sharemyloc;

import android.os.Bundle;
import android.os.SystemClock;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationUpdater {
    private final GoogleApiClient googleApiClient;
    private final LocationListener locationListener;
    private LocationRequest locationRequest;
    private static final FusedLocationProviderApi LOCATION_API = LocationServices.FusedLocationApi;

    private static final LocationRequest FOREGROUND_LOCATION_REQUEST = new LocationRequest()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setSmallestDisplacement(5)
            .setInterval(5000)
            .setFastestInterval(5000);

    private static final LocationRequest BACKGROUND_LOCATION_REQUEST = new LocationRequest()
            .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
            .setSmallestDisplacement(10)
            .setInterval(30000)
            .setFastestInterval(10000);

    public LocationUpdater(final LocationListener locationListener) {
        this.locationListener = locationListener;

        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(App.getContext()).addApi(LocationServices.API);
        builder.addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override public void onConnected(Bundle bundle) {
                LOCATION_API.requestLocationUpdates(googleApiClient, locationRequest, locationListener);
            }

            @Override public void onConnectionSuspended(int i) {}
        });

        builder.addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
            @Override public void onConnectionFailed(ConnectionResult connectionResult) {}
        });

        googleApiClient = builder.build();
    }

    public void startUpdate() {
        stopUpdate();
        locationRequest = FOREGROUND_LOCATION_REQUEST;
        googleApiClient.connect();
    }

    public void startUpdateInBackground() {
        stopUpdate();
        locationRequest = BACKGROUND_LOCATION_REQUEST;
        locationRequest.setExpirationDuration(SharingManager.getExpiration() - SystemClock.currentThreadTimeMillis());
        googleApiClient.connect();
    }

    public void stopUpdate() {
        if (googleApiClient.isConnected()) {
            LOCATION_API.removeLocationUpdates(googleApiClient, locationListener);
            googleApiClient.disconnect();
        }
    }
}
