package io.github.sharemyloc;

import android.util.Log;

public class Logger {
    private static final String TAG = "myApp";

    public static void debug(String message, Object... args) {
        Log.d(TAG, String.format(message, args));
    }

    public static void info(String message, Object... args) {
        Log.i(TAG, String.format(message, args));
    }

    public static void error(String message, Object... args) {
        Log.e(TAG, String.format(message, args));
    }
}
