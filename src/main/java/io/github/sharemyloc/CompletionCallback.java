package io.github.sharemyloc;

public interface CompletionCallback<T> {
    void onComplete(T result);
}
