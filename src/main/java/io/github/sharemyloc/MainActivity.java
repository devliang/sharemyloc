package io.github.sharemyloc;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends FragmentActivity implements LocationListener {
    @Bind(R.id.shareButton) FloatingActionButton shareButton;
    private LocationUpdater locationUpdater;
    private GoogleMap map;
    private boolean isCurrentLocationKnown = false;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setUpMapIfNeeded();
        locationUpdater = new LocationUpdater(this);
    }

    @Override protected void onPause() {
        super.onPause();
        if (!SharingManager.isSharing()) {
            locationUpdater.stopUpdate();
        } else {
            locationUpdater.startUpdateInBackground();
        }
    }

    @Override protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        if (map != null) {
            locationUpdater.startUpdate();
        }
        if (SharingManager.isSharing()) {
            shareButton.setImageResource(android.R.drawable.ic_menu_edit);
        } else {
            shareButton.setImageResource(android.R.drawable.ic_menu_share);
        }
    }

    @Override public void onLocationChanged(Location location) {
        if (location == null) {
            return;
        }

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (!isCurrentLocationKnown) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            isCurrentLocationKnown = true;
        }

        if (SharingManager.isSharing()) {
            FirebaseClient.getInstance(this).updateSession(SharingManager.getSessionId(), latLng);
        }
    }

    @OnClick(R.id.shareButton) public void startShareActivity() {
        SharingManager.startSharing(new CompletionCallback<Boolean>() {
            @Override public void onComplete(Boolean success) {
                if (success) {
                    Intent intent = new Intent(getApplicationContext(), ShareActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void setUpMapIfNeeded() {
        if (map == null) {
            map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            if (map != null) {
                map.setMyLocationEnabled(true);
            }
        }
    }

}
