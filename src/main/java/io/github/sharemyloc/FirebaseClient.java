package io.github.sharemyloc;

import android.content.Context;

import com.firebase.client.Firebase;

public class FirebaseClient extends Firebase {
    private static FirebaseClient instance;
    private static final String SESSION_KEY = "sessions";

    private FirebaseClient(String url) {
        super(url);
    }

    public static FirebaseClient getInstance(Context context) {
        if (instance == null) {
            Firebase.setAndroidContext(context.getApplicationContext());
            instance = new FirebaseClient("https://sharemyloc.firebaseio.com/");
        }
        return instance;
    }

    public void updateSession(String sessionId, Object value) {
        child(SESSION_KEY).child(sessionId).setValue(value);
    }

    public void removeSession(String sessionId) {
        child(SESSION_KEY).child(sessionId).removeValue();
    }

}
