package io.github.sharemyloc;

import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.POST;

public class UrlShortener {

    private static final RequestInterceptor INTERCEPTOR = new RequestInterceptor() {
        @Override public void intercept(RequestFacade request) {
            request.addQueryParam("key", App.getContext().getString(R.string.google_api_key));
        }
    };

    private static final RestAdapter ADAPTER = new RestAdapter.Builder()
            .setEndpoint("https://www.googleapis.com")
            .setRequestInterceptor(INTERCEPTOR)
            .build();

    static Service getService() {
        return ADAPTER.create(UrlShortener.Service.class);
    }

    interface Service {
        @POST("/urlshortener/v1/url")
        void shorten(@Body JsonObject payload, Callback<JsonObject> cb);
    }
}
